---
title: input type="date" /
anchor: date
w3link: https://www.w3schools.com/tags/att_input_type_date.asp
group: 3
example: >-
  &lt;p&gt;What's your birthday? &lt;input type="date" value="1970-01-01" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- This input takes a `value` attribute of the format `YYYY-MM-DD`.
- There is a Rails view helper for this called `date_field_tag`.
- There are also `type="month"`, `type="week"`, and `type="time"` if you want to split out the date fields.
