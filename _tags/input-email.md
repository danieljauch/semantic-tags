---
title: input type="email" /
anchor: email
w3link: https://www.w3schools.com/tags/att_input_type_email.asp
group: 3
example: >-
  &lt;p&gt;What's your email? &lt;input type="email" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- This does browser validation on the email in the field, and will prevent form submission if the `required` attribute is set.
- There's a new attribute with this field called `multiple` where it can validate a list of emails (as long as there is a comma to separate emails).
- There is a Rails view helper for this called `email_field_tag`.
