---
title: datalist
w3link: https://www.w3schools.com/tags/tag_datalist.asp
group: 3
example: >-
  &lt;div&gt;
    &lt;input list="browsers"&gt;
    &lt;datalist id="browsers"&gt;
      &lt;option value="Internet Explorer"&gt;
      &lt;option value="Firefox"&gt;
      &lt;option value="Chrome"&gt;
      &lt;option value="Opera"&gt;
      &lt;option value="Safari"&gt;
    &lt;/datalist&gt;
  &lt;/div&gt;
styles: >-
  datalist {
    display: none;
  }
---

**Notes:**

- From the markup, this is actually a sort of confusing element because the default display is `none`. Then when you realize that it uses the shadow DOM to show a dropdown list, it feels like a `<select>` element, but it's actually even better than that. It's still a text input which means that you can type in whatever you want, but it makes a default input element into an autocomplete function.
- I tried using diacritics, it's not that intelligent.
