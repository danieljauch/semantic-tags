---
title: abbr
w3link: https://www.w3schools.com/tags/tag_abbr.asp
group: 2
example: >-
  &lt;p&gt;The &lt;abbr title="World Health Organization"&gt;WHO&lt;/abbr&gt; was founded in 1948.&lt;/p&gt;
styles: >-
  abbr {
    text-decoration: underline dotted;
  }
---

**Notes:**

- Displays inline by default, but there is no `display` property defined.
