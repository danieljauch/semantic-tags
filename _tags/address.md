---
title: address
w3link: https://www.w3schools.com/tags/tag_address.asp
group: 2
example: >-
  &lt;address&gt;
    Written by &lt;a href="mailto:webmaster@example.com"&gt;Jon Doe&lt;/a&gt;.&lt;br&gt;
    Visit us at:&lt;br&gt;
    Example.com&lt;br&gt;
    Box 564, Disneyland&lt;br&gt;
    USA
  &lt;/address&gt;
styles: >-
  address {
    display: block;
    font-style: italic;
  }
---

**Notes:**

- The address can refer to a web address or a physical address, and if both are present, should wrap the entire block of address-related content.
- If the `<address>` element is inside the `<body>` element, it represents contact information for the document, but if it's inside an `<article>` element, it represents contact information for that article itself.
