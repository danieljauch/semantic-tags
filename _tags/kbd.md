---
title: kbd
w3link: https://www.w3schools.com/tags/tag_kbd.asp
group: 2
example: >-
  &lt;section&gt;
    &lt;header&gt;Keyboard input:&lt;/header&gt;
    &lt;kbd&gt;asdfjkl&lt;/kbd&gt;
  &lt;/section&gt;
styles: >-
  kbd {
    font-family: monospace;
  }
---

**Notes:**

- This tag is somewhat confusingly labelled. `<kbd>` is short for "keyboard input", but there is no input functionality to it, so it's meant to be something that would be inputted by a keyboard, which on a website should kind of be everything. The best usage I can think of is typed content from a user displayed in a unique way, that is not necessarily code. Maybe the preview content for a WYSIWYG editor for example.
