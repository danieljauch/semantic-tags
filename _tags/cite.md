---
title: cite
w3link: https://www.w3schools.com/tags/tag_cite.asp
group: 2
example: >-
  &lt;p&gt;&lt;cite&gt;The Scream&lt;/cite&gt; by Edward Munch. Painted in 1893.&lt;/p&gt;
styles: >-
  cite {
    font-style: italic;
  }
---

**Notes:**

- The `<cite>` tag defines the title of a work (e.g. a book, a song, a movie, a TV show, a painting, a sculpture, etc.).
