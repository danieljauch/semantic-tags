---
title: del&gt;, &lt;ins
anchor: del
w3link: https://www.w3schools.com/tags/tag_del.asp
group: 3
example: >-
  &lt;p&gt;My favorite color is &lt;del&gt;blue&lt;/del&gt; &lt;ins&gt;red&lt;/ins&gt;!&lt;/p&gt;
styles: >-
  del {
    text-decoration: line-through;
  }

  ins {
    text-decoration: underline;
  }
---

**Notes:**

- Displays inline by default, but there is no `display` property defined.
