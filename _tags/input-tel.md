---
title: input type="tel" /
anchor: tel
w3link: https://www.w3schools.com/tags/att_input_type_tel.asp
group: 3
example: >-
  &lt;p&gt;Phone number: &lt;input type="tel" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- As far as I can tell, this input type relies on the server to validate unlike `email` or `url` because of the vast amount of telephone number schemes and the need to allow for letters as well as numbers in telephone numbers.
- There are Rails view helpers for this called `telephone_field_tag` and `phone_field_tag`.
