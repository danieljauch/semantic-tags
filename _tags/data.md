---
title: data
w3link: https://www.w3schools.com/tags/tag_data.asp
group: 3
example: >-
  &lt;ul&gt;
    &lt;li&gt;&lt;data value="21053"&gt;Cherry Tomato&lt;/data&gt;&lt;/li&gt;
    &lt;li&gt;&lt;data value="21054"&gt;Beef Tomato&lt;/data&gt;&lt;/li&gt;
    &lt;li&gt;&lt;data value="21055"&gt;Snack Tomato&lt;/data&gt;&lt;/li&gt;
  &lt;/ul&gt;
styles: >-
  [none]
---

**Notes:**

- From the markup, this is actually a sort of confusing element because the default display is `none`. Then when you realize that it uses the shadow DOM to show a dropdown list, it feels like a `<select>` element, but it's actually even better than that. It's still a text input which means that you can type in whatever you want, but it makes a default input element into an autocomplete function.
- I tried using diacritics, it's not that intelligent.
