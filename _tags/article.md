---
title: article
w3link: https://www.w3schools.com/tags/tag_article.asp
group: 2
example: >-
  &lt;article&gt;
    &lt;h1&gt;Google Chrome&lt;/h1&gt;
    &lt;p&gt;Google Chrome is a free, open-source web browser developed by Google, released in 2008.&lt;/p&gt;
  &lt;/article&gt;
styles: >-
  article {
    display: block;
  }
---
