---
title: nav
w3link: https://www.w3schools.com/tags/tag_nav.asp
group: 2
example: >-
  &lt;nav&gt;
    &lt;ul&gt;
      &lt;li&gt;
        &lt;a href="/"&gt;Home&lt;/a&gt;
      &lt;/li&gt;
      &lt;li&gt;
        &lt;a href="/about"&gt;About&lt;/a&gt;
      &lt;/li&gt;
      &lt;li&gt;
        &lt;a href="/contact"&gt;Contact&lt;/a&gt;
      &lt;/li&gt;
    &lt;/ul&gt;
  &lt;/nav&gt;
styles: >-
  nav {
    display: block;
  }
---
