---
title: wbr
w3link: https://www.w3schools.com/tags/tag_wbr.asp
group: 3
example: >-
  &lt;p&gt;Supercalifragilistic&lt;wbr&gt;expialidocious&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- This is so much easier than trying to help CSS understand how words should break and looks much nicer than wrapping `<span>`s around things to achieve the same means.
