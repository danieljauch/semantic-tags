---
title: header
w3link: https://www.w3schools.com/tags/tag_header.asp
group: 1
example: >-
  &lt;article&gt;
    &lt;header&gt;
      &lt;h1&gt;Most important heading here&lt;/h1&gt;
      &lt;h3&gt;Less important heading here&lt;/h3&gt;
      &lt;p&gt;Some additional information here&lt;/p&gt;
    &lt;/header&gt;
    &lt;p&gt;Lorem ipsum dolor set amet....&lt;/p&gt;
  &lt;/article&gt;
styles: >-
  header {
    display: block;
  }
---

**Notes:**

- Wants an `<h*>` tag inside it.
