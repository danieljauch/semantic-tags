---
title: aside
w3link: https://www.w3schools.com/tags/tag_aside.asp
group: 1
example: >-
  &lt;article&gt;
    &lt;p&gt;My family and I visited The Epcot center this summer.&lt;/p&gt;
    &lt;aside&gt;
      &lt;h4&gt;Epcot Center&lt;/h4&gt;
      &lt;p&gt;The Epcot Center is a theme park in Disney World, Florida.&lt;/p&gt;
    &lt;/aside&gt;
  &lt;/article&gt;
styles: >-
  aside {
    display: block;
  }
---
