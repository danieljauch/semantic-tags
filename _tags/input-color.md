---
title: input type="color" /
anchor: color
w3link: https://www.w3schools.com/tags/att_input_type_color.asp
group: 3
example: >-
  &lt;p&gt;Select your favorite color: &lt;input type="color" value="#ff0000" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- The `value` attribute is what defines the color, just like any other input field, and only takes hex color format.
- There is a Rails view helper for this called `color_tag`.
