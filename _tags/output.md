---
title: output
w3link: https://www.w3schools.com/tags/tag_output.asp
group: 3
example: >-
  &lt;form oninput="z.value = parseInt(x.value) + parseInt(y.value);"&gt;
    &lt;div&gt;  &lt;input type="number" id="x" /&gt;&lt;/div&gt;
    &lt;div&gt;+ &lt;input type="number" id="y" /&gt;&lt;/div&gt;
    &lt;/hr&gt;
    &lt;div&gt;= &lt;output type="number" name="z" for="x y"&gt;&lt;/output&gt;&lt;/div&gt;
  &lt;/form&gt;
styles: >-
  output {
    display: inline;
  }
---

**Notes:**

- To understand how this really functions, I highly recommend using the [Tryit Editor](https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_output) on the W3Schools link below.
- Will require a `for` attribute listing the `id`'s of all referenced inputs that this `<output>` is the result of.
