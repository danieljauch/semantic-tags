---
title: time
w3link: https://www.w3schools.com/tags/tag_time.asp
group: 2
example: >-
  &lt;p&gt;I have a date on &lt;time datetime="2008-02-14 20:00"&gt;Valentines day&lt;/time&gt;.&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- The `datetime` attribute isn't required, but helps with machine reading and add-to-calendar functionality on many devices.
- This has a Rails helper called `time_tag`.
