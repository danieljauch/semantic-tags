---
title: optgroup
w3link: https://www.w3schools.com/tags/tag_optgroup.asp
group: 3
example: >-
  &lt;select&gt;
    &lt;optgroup label="Swedish Cars"&gt;
      &lt;option value="volvo"&gt;Volvo&lt;/option&gt;
      &lt;option value="saab"&gt;Saab&lt;/option&gt;
    &lt;/optgroup&gt;
    &lt;optgroup label="German Cars"&gt;
      &lt;option value="mercedes"&gt;Mercedes&lt;/option&gt;
      &lt;option value="audi"&gt;Audi&lt;/option&gt;
    &lt;/optgroup&gt;
  &lt;/select&gt;
styles: >-
  [none]
---

**Notes:**

- This is purely browser implemented, so make sure you test between different browsers. The functionality is consistent, but the look varies between OS's.
