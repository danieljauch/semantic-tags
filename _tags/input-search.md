---
title: input type="search" /
anchor: search
w3link: https://www.w3schools.com/tags/att_input_type_search.asp
group: 3
example: >-
  &lt;p&gt;Search: &lt;input type="search" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- Essentially, this is a text input, but it takes advantage of a lot of new attributes to HTML5 like `autocomplete` and `autofocus`.
- There is a Rails view helper for this called `search_field_tag`.
