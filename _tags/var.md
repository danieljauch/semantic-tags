---
title: var
w3link: https://www.w3schools.com/tags/tag_var.asp
group: 2
example: >-
  &lt;code&gt;
    &lt;div&gt;let &lt;var&gt;x&lt;/var&gt; = 0;&lt;/div&gt;
  &lt;/code&gt;
styles: >-
  var {
    font-style: italic;
  }
---

**Notes:**

- This is meant for a variable name. Since it doesn't define the `font-family` in any way, it's actually meant to be extra decoration _inside_ the `<code>` tag, not a replacement for a block of code.
