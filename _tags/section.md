---
title: section
w3link: https://www.w3schools.com/tags/tag_section.asp
group: 1
example: >-
  &lt;section&gt;
    &lt;header&gt;
      &lt;h3&gt;WWF&lt;/h3&gt;
    &lt;/header&gt;
    &lt;p&gt;The World Wide Fund for Nature (WWF) is....&lt;/p&gt;
  &lt;/section&gt;
styles: >-
  section {
    display: block;
  }
---

**Notes:**

- Wants a `<header>` tag inside it.
