---
title: pre
w3link: https://www.w3schools.com/tags/tag_cite.asp
group: 2
example: >-
  &lt;pre&gt;
    Text in a pre element
    is displayed in a fixed-width
    font, and it preserves
    both      spaces and
    line breaks
  &lt;/pre&gt;
styles: >-
  pre {
    display: block;
    font-family: monospace;
    white-space: pre;
    margin: 1em 0;
  }
---

**Notes:**

- This is one of the most powerful semantic tags as it gives the content control over the whitespace which is a fairly uniquely difficult thing to achieve in many apps, especially when it comes to our markup. However, this power comes primarily from the default style of `white-space: pre` which could theoretically be applied to any element that needs this functionality.
