---
title: mark
w3link: https://www.w3schools.com/tags/tag_mark.asp
group: 2
example: >-
  &lt;p&gt;Do not forget to buy &lt;mark&gt;milk&lt;/mark&gt; today.&lt;/p&gt;
styles: >-
  mark {
    background-color: yellow;
    color: black;
  }
---

**Notes:**

- Many search libraries like [`markjs`](https://markjs.io/) actually use this tag since the behavior exists by default. It's like a pseudo `::selection` that you can apply to markup.
