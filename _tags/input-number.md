---
title: input type="number" /
anchor: number
w3link: https://www.w3schools.com/tags/att_input_type_number.asp
group: 3
example: >-
  &lt;p&gt;What's your favorite number? &lt;input type="number" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- Other inputs include these attributes, but you can set a `min` attribute for minimum, `max` for maximum, and `step` to dictate how the "spinner" will tick up or down the value.
