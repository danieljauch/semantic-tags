---
title: code
w3link: https://www.w3schools.com/tags/tag_code.asp
group: 2
example: >-
  &lt;section&gt;
    &lt;header&gt;JavaScript:&lt;/header&gt;
    &lt;code&gt;let js = "good";&lt;/code&gt;
  &lt;/section&gt;
styles: >-
  code {
    font-family: monospace;
  }
---
