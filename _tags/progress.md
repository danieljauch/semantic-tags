---
title: progress
w3link: https://www.w3schools.com/tags/tag_progress.asp
group: 3
example: >-
  &lt;progress value="22" max="100"&gt;&lt;/progress&gt;
styles: >-
  progress {
    -webkit-appearance: progress-bar;
    box-sizing: border-box;
    display: inline-block;
    height: 1em;
    width: 10em;
    vertical-align: -0.2em;
  }
---

**Notes:**

- `<meter>` and `<progress>` seem like the same element, but `<meter>` has more options. The _technical_ difference is that `<meter>` is meant to represent a static value for where something currently stands, like a stock of a product, whereas `<progress>` is meant to be a measure of what's currently happening like progress on filling out a multi-part form.
- This is somewhat consistently styled between browsers, which is nice.
- It's easy enough to override for custom styling, and depending on how you do it, it actually keeps a lot of the functionality. Worth playing with further.
- You can include content between the open and close tags, but it won't show unless you override the styling with `-webkit-appearance: none;`.
