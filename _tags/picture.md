---
title: picture
w3link: https://www.w3schools.com/tags/tag_picture.asp
group: 3
example: >-
  &lt;picture&gt;
    &lt;source media="(min-width: 650px)" srcset="img_pink_flowers.jpg" /&gt;
    &lt;source media="(min-width: 465px)" srcset="img_white_flower.jpg" /&gt;
    &lt;img src="img_orange_flowers.jpg" alt="Flowers" /&gt;
  &lt;/picture&gt;
styles: >-
  [none]
---

**Notes:**

- If you pick one item from this category to start with learning, it's this one.
- The `media` attribute for source can include **any valid media query** which means you can have alternates for grayscale, audio-only, high DPI, breakpoints, print view, or anything else you can think of.
- The `<img>` element **must** be last for this to work.
- I noticed an flash of the `<img>`'s source before the other images load in in certain cases, so to avoid that, you may want to set up some sort of lazy-loading procedure if you experience this.
- `<picture>` displays inline by default.
- You cannot style `<source>` because it's not an actual element in the same way that the `<img>` is. Style only the `<img>` and it's source is changing based on the query.
