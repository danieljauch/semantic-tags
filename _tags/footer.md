---
title: footer
w3link: https://www.w3schools.com/tags/tag_footer.asp
group: 1
example: >-
  &lt;footer&gt;
    &lt;p&gt;Posted by: Hege Refsnes&lt;/p&gt;
    &lt;p&gt;Contact information: &lt;a href="mailto:someone@example.com"&gt;
    someone@example.com&lt;/a&gt;.&lt;/p&gt;
  &lt;/footer&gt;
styles: >-
  footer {
    display: block;
  }
---
