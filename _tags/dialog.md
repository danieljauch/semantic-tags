---
title: dialog
w3link: https://www.w3schools.com/tags/tag_dialog.asp
group: 3
example: >-
  &lt;p&gt;
    &lt;dialog open&gt;This is an open dialog window&lt;/dialog&gt;
  &lt;/p&gt;
styles: >-
  dialog {
    display: block;
    position: absolute;
    left: 0px;
    right: 0px;
    width: -webkit-fit-content;
    height: -webkit-fit-content;
    color: black;
    margin: auto;
    border-width: initial;
    border-style: solid;
    border-color: initial;
    border-image: initial;
    padding: 1em;
    background: white;
  }
---

**Notes:**

- This one is sort of an honorable mention rather than a serious suggestion to the workflow yet. I would suggest using it for things like alerts rather than the browser ones for styling and UI reasons, but you will need to use JavaScript to open / close.
- Caveat: This has the lowest browser support and needs a polyfill if you want to use it.
- There's a really good guide to what's available [here](https://www.viget.com/articles/the-dialog-element/)
