---
title: details&gt;, &lt;summary
anchor: details
w3link: https://www.w3schools.com/tags/tag_details.asp
group: 3
example: >-
  &lt;details&gt;
    &lt;summary&gt;Click to see more&lt;/summary&gt;
    &lt;p&gt;Ut consectetur amet ullamco est deserunt.&lt;/p&gt;
  &lt;/details&gt;
styles: >-
  details,
  summary {
    display: block;
  }
---

**Notes:**

- It's a browser-based accordion! Pretty cool options. It uses an inline attribute called `open`, so you can do only-one-at-a-time things fairly easily in JavaScript and with a few positioning tricks, it makes for a very quick tab setup.
- It's a little confusing, but basically anything that _isn't_ a `<summary>` tag goes away (including content within `<details>`, but above `<summary>`), and not with a `display: none` or any other CSS property, so the only way to transition is using an animation and it will only animate in, not out.
- If you have two or more `<summary>` tags, only the first one does anything. This makes a lot of sense, but because the way these elements are nested is kind of different, it can be hard to troubleshoot issues where a `<summary>` isn't showing up or responding because all of the interaction takes place in the spooky shadow DOM.
