---
title: q
w3link: https://www.w3schools.com/tags/tag_q.asp
group: 2
example: >-
  &lt;p&gt;
    &lt;span&gt;WWF's goal is to: &lt;/span&gt;
    &lt;q&gt;Build a future where people live in harmony with nature.&lt;/q&gt;
    &lt;span&gt;We hope they succeed.&lt;/span&gt;
  &lt;/p&gt;
styles: >-
  q {
    display: inline;
  }

  q::before {
    content: open-quote;
  }

  q::after {
    content: close-quote;
  }
---

**Notes:**

- A potential "gotcha" with this one is that it relies on `::before` and `::after` pseudo-elements to display the surrounding quotes, so anything that edits those will remove the quotes. Use a wrapper `<div>` to style or decorate any surroundings. However, this also makes for a really easy customization for customization of the quotes if you want to change font styles.
- Don't type quotes into the tag if you're using the default pseudo quotes, otherwise you'll end up with duplication.
