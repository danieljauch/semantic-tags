---
title: samp
w3link: https://www.w3schools.com/tags/tag_samp.asp
group: 2
example: >-
  &lt;section&gt;
    &lt;header&gt;Sample output:&lt;/header&gt;
    &lt;samp&gt;Sample output from a computer program&lt;/samp&gt;
  &lt;/section&gt;
styles: >-
  samp {
    font-family: monospace;
  }
---

**Notes:**

- `<samp>` suffers from the same problem as `<kbd>` where it's very vaguely named. "Sample output" is meant to display the _output_ of a computer program rather than the code input that might be displayed in a `<code>` tag. Many times you'll use this as a pair of tags. input : output :: `<code>` : `<samp>`
