---
title: blockquote
w3link: https://www.w3schools.com/tags/tag_blockquote.asp
group: 2
example: >-
  &lt;blockquote cite="http://www.worldwildlife.org/who/index.html"&gt;
    For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.
  &lt;/blockquote&gt;
styles: >-
  blockquote {
    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 40px;
    margin-right: 40px;
  }
---

**Notes:**

- Make sure to include a `cite` attribute so that the site's content can remain canonical.
- This element is called blockquote because it's meant for content using `display: block;`. If you would prefer an inline quotation, see `<q>`.
- Also, don't confuse this with `<cite>` which is meant for the citation of the _title_ of a work whereas this is citing the _content_ of the work.
