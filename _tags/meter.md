---
title: meter
w3link: https://www.w3schools.com/tags/tag_meter.asp
group: 3
example: >-
  &lt;meter value="2" min="0" max="10"&gt;2 out of 10&lt;/meter&gt;
styles: >-
  meter {
    -webkit-appearance: meter;
    box-sizing: border-box;
    display: inline-block;
    height: 1em;
    width: 5em;
    vertical-align: -0.2em;
    -webkit-user-modify: read-only !important;
  }
---

**Notes:**

- `<meter>` and `<progress>` seem like the same element, but `<meter>` has more options. The _technical_ difference is that `<meter>` is meant to represent a static value for where something currently stands, like a stock of a product, whereas `<progress>` is meant to be a measure of what's currently happening like progress on filling out a multi-part form.
- There are `low`, `optimum`, and `high` properties to set "ideal" on this.
- This is somewhat consistently styled between browsers, which is nice.
- It's easy enough to override for custom styling, however as soon as you override the styling, you need to recreate the functionality with something like a background gradient or something, which isn't ideal.
- You can include content between the open and close tags, but it won't show unless you override the styling with `-webkit-appearance: none;`.
