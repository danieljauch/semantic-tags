---
title: input type="url" /
anchor: url
w3link: https://www.w3schools.com/tags/att_input_type_url.asp
group: 3
example: >-
  &lt;p&gt;Website: &lt;input type="url" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- Validation for this field requires a fully qualified URL, so you'll need to use a text field for relative paths or any kind of shorthand URL.
- There is a Rails view helper for this called `url_field_tag`.
