---
title: dl&gt;, &lt;dt&gt;, &lt;dd
anchor: dl
w3link: https://www.w3schools.com/tags/tag_dl.asp
group: 3
example: >-
  &lt;dl&gt;
    &lt;dt&gt;Coffee&lt;/dt&gt;
    &lt;dd&gt;Black hot drink&lt;/dd&gt;
    &lt;dt&gt;Milk&lt;/dt&gt;
    &lt;dd&gt;White cold drink&lt;/dd&gt;
  &lt;/dl&gt;
styles: >-
  dl {
    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 0;
    margin-right: 0;
  }

  dt {
    display: block;
  }

  dd {
    display: block;
    margin-left: 40px;
  }
---

**Notes:**

- This is kind of a combination between a list and a table. It's single-column in table terms, but `<dt>` acts like `<th>`, so you have headers inside the list itself.
- Unlike a normal list, you can essentially house multiple lists inside one `<dl>`.
- All of this behavior can be easily achieved with a list or table, but this is vastly less markup for the same outcome.
