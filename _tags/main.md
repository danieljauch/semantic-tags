---
title: main
w3link: https://www.w3schools.com/tags/tag_main.asp
group: 1
example: >-
  &lt;main&gt;
    &lt;h1&gt;Web Browsers&lt;/h1&gt;
    &lt;p&gt;Google Chrome, Firefox, and Internet Explorer are the most used browsers today.&lt;/p&gt;
    &lt;article&gt;
      &lt;h1&gt;Google Chrome&lt;/h1&gt;
      &lt;p&gt;Google Chrome is a free, open-source web browser developed by Google,
      released in 2008.&lt;/p&gt;
    &lt;/article&gt;
    &lt;article&gt;
      &lt;h1&gt;Internet Explorer&lt;/h1&gt;
      &lt;p&gt;Internet Explorer is a free web browser from Microsoft, released in 1995.&lt;/p&gt;
    &lt;/article&gt;
    &lt;article&gt;
      &lt;h1&gt;Mozilla Firefox&lt;/h1&gt;
      &lt;p&gt;Firefox is a free, open-source web browser from Mozilla, released in 2004.&lt;/p&gt;
    &lt;/article&gt;
  &lt;/main&gt;
styles: >-
  main {
    display: block;
  }
---

**Notes:**

- There must not be more than one `<main>` element in a document. The `<main>` element must *not* be a descendant of an `<article>`, `<aside>`, `<footer>`, `<header>`, or `<nav>` element. It's ideally a direct child of `<body>` or some kind of content wrapper using a `<section>`.
