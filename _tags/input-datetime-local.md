---
title: input type="datetime-local" /
anchor: datetime-local
w3link: https://www.w3schools.com/tags/att_input_type_datetime-local.asp
group: 3
example: >-
  &lt;p&gt;What time is the event? &lt;input type="datetime-local" value="1970-01-01T14:30" /&gt;&lt;/p&gt;
styles: >-
  [none]
---

**Notes:**

- This input takes a `value` attribute of the format `YYYY-MM-DDTHH:MM`.
- Even though there is a "local" time included in this, there isn't a _locale_ associated with it. Therefore, if you take a time out of this, it will show in GMT by default.
- There is a Rails view helper for this called `datetime_local_field_tag`.
