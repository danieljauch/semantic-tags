---
title: figure&gt;, &lt;figcaption
anchor: figure
w3link: https://www.w3schools.com/tags/tag_figure.asp
group: 2
example: >-
  &lt;figure&gt;
    &lt;img src="pic_trulli.jpg" alt="Trulli"&gt;
    &lt;figcaption&gt;Fig.1 - Trulli, Puglia, Italy.&lt;/figcaption&gt;
  &lt;/figure&gt;
styles: >-
  figure {
    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 40px;
    margin-right: 40px;
  }

  figcaption {
    display: block;
  }
---
