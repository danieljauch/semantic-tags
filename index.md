---
layout: page
title: Start here!
aside: false
---

<section class="block">
  <h4>Click through each step</h4>

  <ul>
    <li><a href="/step-1">Step 1</a></li>
    <li><a href="/step-2">Step 2</a></li>
    <li><a href="/step-3">Step 3</a></li>
  </ul>

  <h4>See the hover-over examples</h4>

  <ul>
    <li><a href="/hover-example">Hover examples</a></li>
  </ul>

  <h4>Visit links</h4>

  <ul>
    {% for link in site.data.footer %}
      <li>
        <a href="{{ link.url }}">{{ link.text }}</a>
      </li>
    {% endfor %}
  </ul>

  <h4>Use your knowledge!</h4>
</section>
