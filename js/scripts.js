function positionTOC() {
  var toc = $('#toc_block');
  var offset = $('.site__aside').offset().top;
  var windowScroll = $(window).scrollTop();
  var startScrollPosition = offset;

  if (windowScroll > startScrollPosition) {
    toc.css({ top: windowScroll - offset });
  } else {
    toc.css({ top: 0 });
  }
}

$(document).ready(function() {
  positionTOC();

  $(window).scroll(function() {
    positionTOC();
  });

  $('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#top"]')
    .click(function(e) {
      $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top - 16
      }, 250);
    });

  $('a[href="#top"]')
    .click(function(e) {
      $('html, body').animate({
        scrollTop: 0
      }, 250);
    });
});
