# Globals:

**Purpose:** Reset all CSS defaults, prepare all tags for standard styling without issue. Does not use any classes, only tags to set global defaults.
**Preferred load order:** Reset, body, html5, [alphabetical from here / no preferred order]
**Global load order:** Second, after presets